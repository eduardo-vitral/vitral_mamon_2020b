"""
Created on 2020

@author: Eduardo Vitral

"""
###############################################################################
#
# 2020, Paris
#
# This file reads the HST data from program GO-10775 and converts the master
# frame back to sky positions in (RA,Dec). Then, it filters the proper motions
# according to Vitral & Mamon 2020b, section 2.4 and filters the color-
# magnitude diagram of the cluster through a Kernel Density Estimation.
# Finally, it saves the information in a .dat file. This code was designed 
# for NGC 6397, other clusters information should be inserted in the 
# "Parameters definition" section if the user seeks a more general treatment.
#
# Documentation is provided on Vitral & Mamon, 2020b. 
# If you have any further questions please email vitral@iap.fr
#
###############################################################################


from astropy.io import fits
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import LogNorm
import matplotlib.lines as mlines
import glob, os
import numpy as np
from scipy.stats import gaussian_kde
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import matplotlib.ticker as ticker

plt.rcParams['font.family'] = 'serif'
plt.rcParams['font.serif'] = ['Computer Modern Roman'] \
    + plt.rcParams['font.serif']
    
from matplotlib import rc
rc('text', usetex=True)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Interactive section"
#------------------------------------------------------------------------------

string = "This function was written for the analysis of NGC 6397. " + \
         r"For other globular clusters, the user is invited to fill its" + \
         r" respective values according to the code section" + \
         r" 'Parameters definition'. Note that the .fits file used must " + \
         r"be correctly labeled, please go to the 'FITS file reading' " + \
         r"section of this code to see it.  "
         
print(string)

# Get directory name
string = "Enter the path of your HST .fits file: " + \
         "\n Ex.: /home/usr/DATA/HST.fits \n"
hst_file_path = input(string)

string = "Enter the last digits of the source NGC catalog: " + \
         "\n Ex.: 6397 for NGC6397 \n"
file_name = input(string)

string = "Run on debug mode? (y/n) \n"
debug = input(string)
if (debug == 'y') :
    debug = True
else :
    debug = False

string = "Fraction of PM error (eq. 2 of Vitral & Mamon 2020b): " + \
         "\n Accepted values are 0.25, 0.5 and 1.0 \n"
err_fac = float(input(string))

string = "Enter the n-sigma contour level to be considered:" + \
         "\n Ex.: 1, 2 or 3. \n"
nsig   = int(3 - float(input(string)))

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Parameters definition"
#------------------------------------------------------------------------------

# Type here the magnitude cut you want to apply to the data
mag_limit = 30
        
if ("6397" in file_name) :
    ra_source  = 265.175375 # deg
    dec_source = -53.674333 # deg
    pm_disp    = 0.394 # mas yr-1
    Rcut       = 0.0277 # deg
    PMcut      = 3 # mas yr-1
    pmra_gc    = 3.306 # mas yr-1
    pmdec_gc   = -17.618 # mas yr-1

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Miscellaneous parameters"
#------------------------------------------------------------------------------       

if (err_fac == 0.5) :
    err_str = 'half'
elif (err_fac == 0.25) :
    err_str = 'quart'
elif (err_fac == 1.0) :
    err_str = 'full'
    
my_grey = (240/255,240/255,240/255)
  
# Creates personal colormaps in shades of gray, yellow and red
n_bins    = 1000
colors    = [(0,0,50/255),(102/255,178/255,1),
             (1,102/255,102/255),(1,1,1)]
cmap_name = 'my_rainbow'
my_rainbow1  = LinearSegmentedColormap.from_list(cmap_name,colors,N=n_bins)
  
# Creates personal colormaps in shades of gray, yellow and red
n_bins    = 1000
colors    = [(240/255,240/255,240/255),(0,0,50/255),(102/255,178/255,1),
             (1,102/255,102/255),(1,1,1)]
cmap_name = 'my_rainbow'
my_rainbow2   = LinearSegmentedColormap.from_list(cmap_name,colors,N=n_bins)
        
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"General functions"
#------------------------------------------------------------------------------

def count_function(C) :
    C = np.asarray(C)
    if (len(C) <1) :
        return np.nan
    val = len(C)
    return val

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Spherical trigonometry functions"
#------------------------------------------------------------------------------

# Gets the angle between one given direction and the RA direction
def getPhi(ra,dec) :
    # Converts angles to radians
    ra0  = ra
    dec0 = np.asarray([dec_source for i in range(len(dec))])
    
    R0 = getDistanceDeg(ra0, dec0, ra_source, dec_source) * np.pi/180
    
    R  = getDistanceDeg(ra, dec, ra_source, dec_source) * np.pi/180
    
    ra  = ra * np.pi/180
    dec = dec * np.pi/180
    
    ra0  = ra0 * np.pi/180
    dec0 = dec0 * np.pi/180
    
    sinP  = np.cos(dec) * np.sin(ra - (ra_source * np.pi/180)) / np.sin(R)
    
    sinP0 = np.cos(dec0) * np.sin(ra0 - (ra_source * np.pi/180)) / np.sin(R0)
    
    sinP0[np.where(sinP0 > 1)] = 1
    
    if (sinP[len(sinP)-1] > 0) :
        if (dec[len(dec)-1] > (dec_source * np.pi/180)) :
            P = np.arcsin(sinP)
        else :
            P = np.pi - np.arcsin(sinP)
    else :
        if (dec[len(dec)-1] > (dec_source * np.pi/180)) :
            P = np.arcsin(sinP)
        else :
            P = np.pi - np.arcsin(sinP)
            
    if (sinP0[len(sinP0)-1] > 0) :
        if (dec0[len(dec0)-1] > (dec_source * np.pi/180)) :
            P0 = np.arcsin(sinP0)
        else :
            P0 = np.pi - np.arcsin(sinP0)
    else :
        if (dec0[len(dec0)-1] > (dec_source * np.pi/180)) :
            P0 = np.arcsin(sinP0)
        else :
            P0 = np.pi - np.arcsin(sinP0)
        
    DP = P - P0
    
    return DP

# Gets distance in degrees from sky positions in radians
def getDistanceDeg(ra1,dec1,ra2,dec2) :
    ra1  = ra1 * np.pi/180
    ra2  = ra2 * np.pi/180
    dec1 = dec1 * np.pi/180
    dec2 = dec2 * np.pi/180
    
    cosR = np.sin(dec1) * np.sin(dec2) + np.cos(dec1) * np.cos(dec2) * \
           np.cos(ra2-ra1)
    R    = np.arccos(cosR) * 180/np.pi
    
    return R

# Returns separation angle in radians between two 3D arrays
def getAngSep(v0,v) :
    
    try:
        v0.shape
    except NameError:
        print("You did not give a valid input.")
        return
    v    = v / np.linalg.norm(v)
    B    = np.arcsin(v[2])
    cosA = v[0]/np.cos(B)
    sinA = v[1]/np.cos(B)
    
    if (cosA > 0 and sinA >= 0) :
        A = np.arcsin(sinA)
    if (cosA <= 0 and sinA > 0) :
        A = np.arccos(cosA)
    if (cosA < 0 and sinA <= 0) :
        A = 2*np.pi - np.arccos(cosA)
    if (cosA >= 0 and sinA < 0) :
        A = np.arcsin(sinA)
    
    v0    = v0 / np.linalg.norm(v0)
    B0    = np.arcsin(v0[2])
    cosA0 = v0[0]/np.cos(B0)
    sinA0 = v0[1]/np.cos(B0)
    
    if (cosA0 > 0 and sinA0 >= 0) :
        A0 = np.arcsin(sinA0)
    if (cosA0 <= 0 and sinA0 > 0) :
        A0 = np.arccos(cosA0)
    if (cosA0 < 0 and sinA0 <= 0) :
        A0 = 2*np.pi - np.arccos(cosA0)
    if (cosA0 >= 0 and sinA0 < 0) :
        A0 = np.arcsin(sinA0)
        
    cosR = np.sin(B0) * np.sin(B) + np.cos(B0) * np.cos(B) * np.cos(A-A0)
    R    = np.arccos(cosR)
    
    return R
 
# Returns the rotation of v of an angle theta with respect to the vector k
# Applies the Rodrigues formula from:
# https://en.wikipedia.org/wiki/Rodrigues%27_rotation_formula
def getRodriguesForm(k,v,theta) :
    
    try:
        v.shape
    except NameError:
        print("You did not give a valid input for the Rodrigues formula.")
        return
    
    if (len(np.shape(v)) == 1 and len(v) == 3) :
        v_rot = v * np.cos(theta) + np.cross(k,v) * np.sin(theta) + \
                k * np.dot(k,v) * (1 - np.cos(theta))
        
    elif (len(np.shape(v)) == 2 and np.shape(v)[0] == 3) :
        v_rot = np.zeros((np.shape(v)[1],3))
        for i in range(0,len(v_rot)) :
        
            v0       = np.asarray([v[0][i],v[1][i],v[2][i]])
            v_rot[i] = v0 * np.cos(theta) \
                     + np.cross(k,v0) * np.sin(theta) \
                     + k * np.dot(v0,k) * (1 - np.cos(theta))
                     
            if (debug == True and i < 10) :
                print('v0   :', v0)
                print('v_rot:', v_rot[i])
    else :
        print("You did not give a valid input for the Rodrigues formula.")
        return
    
    return v_rot

# Gets new angles in degrees of a rotated vector
def getCoords(v_i,v0_i,v0_f,*args) :
    
    if ((v0_i == v0_f).all()) :
        if (debug == True) :
            print('Pure rotation')
        k     = v0_i / np.linalg.norm(v0_i)
        try:
            theta = args[0]
        except NameError:
            ('You did not provide an angle to rotate. Default is zero.')
            theta = 0
    else :
        if (debug == True) :
            print('Translation in spherical geometry')
        k     = np.cross(v0_i,v0_f) / np.linalg.norm(np.cross(v0_i,v0_f))
        theta = getAngSep(v0_i,v0_f)
    
    if (debug == True) :
        print('Vector k:', k)
        print('Angle of separation [degrees]:', theta * 180/np.pi)
    
    v_f = getRodriguesForm(k,v_i,theta)
    
    try:
        v_f.shape
    except NameError:
        print("You did not give a valid input for the Rodrigues formula.")
        return
    
    if (len(np.shape(v_f)) == 1 and len(v_f) == 3) :
        v_f  = v_f / np.linalg.norm(v_f)
        B    = np.arcsin(v_f[2])
        cosA = v_f[0]/np.cos(B)
        sinA = v_f[1]/np.cos(B)
        
        if (cosA > 0 and sinA >= 0) :
            A = np.arcsin(sinA)
        if (cosA <= 0 and sinA > 0) :
            A = np.arccos(cosA)
        if (cosA < 0 and sinA <= 0) :
            A = (np.pi - np.arccos(cosA)) + np.pi
        if (cosA >= 0 and sinA < 0) :
            A = np.arcsin(sinA)
    
    elif (len(np.shape(v_f)) == 2) :
        A = np.zeros(len(v_f))
        B = np.zeros(len(v_f))
        for i in range(0,len(v_f)) :
            v_f[i]  = v_f[i] / np.linalg.norm(v_f[i])
            B[i]    = np.arcsin(v_f[i][2])
            cosA    = v_f[i][0]/np.cos(B[i])
            sinA    = v_f[i][1]/np.cos(B[i])
            
            if (cosA > 0 and sinA >= 0) :
                A[i] = np.arcsin(sinA)
            if (cosA <= 0 and sinA > 0) :
                A[i] = np.arccos(cosA)
            if (cosA < 0 and sinA <= 0) :
                A[i] = (np.pi - np.arccos(cosA)) + np.pi
            if (cosA >= 0 and sinA < 0) :
                A[i] = np.arcsin(sinA)
            
            if (debug == True and i < 10) :
                print('A, B [degrees]:', A[i]*(180/np.pi), B[i]*(180/np.pi))
    else :
        print("You did not give a valid input for the Rodrigues formula.")
        return
    
    
    return A * (180/np.pi), B * (180/np.pi)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"CMD filtering section"
#------------------------------------------------------------------------------
 
# This function filters the CMD data by means of a Kernel Density Estimation.
def CMD_filter (mag1,mag2,siglevels,idxsig,idx) :
    
    x = mag1[idx]-mag2[idx]
    y = mag2[idx]
    
    ranges = [[np.nanmin(x)-0.1,np.nanmax(x)+0.1],
              [np.nanmin(y)-0.5,np.nanmax(y)+0.5]]
    
    data = np.vstack([x,y])

    kde  = gaussian_kde(data,bw_method='silverman')
    
    # evaluate on a regular grid
    xgrid        = np.linspace(ranges[0][0], ranges[0][1], 200)
    ygrid        = np.linspace(ranges[1][0], ranges[1][1], 200)
    Xgrid, Ygrid = np.meshgrid(xgrid, ygrid)
    Zhist        = kde.evaluate(np.vstack([Xgrid.ravel(), Ygrid.ravel()]))
    Z            = Zhist.reshape(Xgrid.shape)
    maxZ         = np.amax(Z)
    
    if (debug == True) :
        integPDF = kde.integrate_box([ranges[0][0],ranges[1][0]],
                                  [ranges[0][1],ranges[1][1]])
        print('\n KDE validation: This value should be very close to 1:', 
              round(integPDF,3))

    levels = maxZ * np.exp(-0.5 * np.array(siglevels)**2)
    # Plot the result as an image
    
    labels = [r'$\mathrm{Isochrone: \ Proper \ motion \ filtered \ data}$',
              r'$\mathrm{Kernel \ density \ estimation}$']
    xlabels = [r"${\mathrm{F606W}} - {\mathrm{F814W}}$",
               r"${\mathrm{F606W}} - {\mathrm{F814W}}$"]
    ylabels = [r"${\mathrm{F814W}}$",
               r""]
    
    fig,axs = plt.subplots(1,2, figsize=(12, 5), facecolor='w', 
                           edgecolor='k', sharey=True, sharex=True)
    
    fig.subplots_adjust(wspace=.25)
    axs = axs.ravel()
    
    c=axs[0].hexbin(x,y,C=np.ones(len(y)),
            cmap=my_rainbow1,gridsize=200,reduce_C_function=count_function,
            extent=[ranges[0][0], ranges[0][1], ranges[1][0], ranges[1][1]])
    cbar0 = fig.colorbar(c, ax = axs[0], format="$%.0f$")
    cbar0.ax.tick_params(labelsize=13) 
    cbar0.set_label('$\mathrm{Star \ counts}$',fontsize = 15, labelpad=15)
    cbar0.locator = ticker.LinearLocator(6)
    cbar0.minorticks_off()
    
    axs[1].imshow(Z,origin='lower',aspect='auto',
              extent=[ranges[0][0], ranges[0][1], ranges[1][0], ranges[1][1]],
              cmap=my_rainbow2, norm=LogNorm(vmin=0.001))
    
    m = plt.cm.ScalarMappable(cmap=my_rainbow1)
    m.set_array(Z)
    m.set_clim(0.01, maxZ)
    cbar = plt.colorbar(m, format="$%.1f$")

    cbar.set_label('$\mathrm{PDF}$',fontsize = 15, labelpad=15)
    cbar.ax.tick_params(labelsize=13)
    cbar.ax.locator_params(tight=True)
    cbar.locator = ticker.LinearLocator(6)
    contours = axs[1].contour(Z, origin='lower',levels=levels, colors='k', 
                           extent=[ranges[0][0], ranges[0][1], ranges[1][0], 
                                   ranges[1][1]])
    c_line  = mlines.Line2D([], [], color='black', lw=2, 
                        label=r"$1,2,3 - \sigma \ \mathrm{contours}$")
    axs[1].legend(handles=[c_line], 
               loc='best', prop={'size': 15})

    
    for i in range(0,len(axs)) :
        
        axs[i].tick_params(labeltop=False, labelright=False, top = True, 
                           right = True, 
                        axis='both', which='major', labelsize=14,
                        direction="in", 
                        length = 8)
        axs[i].tick_params(labeltop=False, labelright=False, top = True, 
                           right = True, 
                        axis='both', which='minor', labelsize=14,
                        direction="in",
                        length = 4)
        axs[i].set_title(labels[i], fontsize = 17, pad = 10)
        axs[i].set_xlabel(xlabels[i], fontsize = 16)
        axs[i].set_ylabel(ylabels[i], fontsize = 16)
        axs[i].set_xlim(0.1, 2.1)
        axs[i].set_ylim(13.1, 22.9)
        axs[i].minorticks_on()
        axs[i].grid()
        axs[i].set_facecolor(my_grey)
    
    axs[0].invert_yaxis()
    
    plt.savefig(r'CMD_hist' + err_str + '.pdf', 
            format = 'pdf', bbox_inches="tight")

    plt.show()
    plt.close(fig)
    
    
    
    lcontours = list()
    inCMD     = list()
    
    p     = contours.collections[idxsig].get_paths()
    npoly = len(p)
    for i in range(0,npoly) :
        v = p[i].vertices
        contourx = v[:,0]
        contoury = v[:,1]
        
        contour_array = np.array([contourx,contoury])
    
        lcontours.append(contour_array)
        inCMD.append(Polygon([(contourx[i],contoury[i]) \
                         for i in range(0,len(contourx))]))
    
    
    idx_final = list()
    for i in idx :
        for j in range(0,npoly) :
            if (inCMD[j].contains(Point(mag1[i]-mag2[i],mag2[i]))) :
                idx_final.append(i)
    idx_final = np.asarray(idx_final).astype(int)    
    
    return idx_final, lcontours
        
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"writing functions"
#------------------------------------------------------------------------------

# This function creates the .dat file to be given to MAMPOSSt-PM
def writeInFile (data) :

    Name = 'NGC6397_HST_' + err_str + '.dat'
    # Erases the files that will be created again, so we don't have repeated 
    #information
    for file in glob.glob("*.dat"):
        if (file in Name) :
            os.remove(file)
    astrometric = open(Name, "w")
    astrometric.write('#RA Dec PMRA ePMRA PMDec ePMDec cPMRA cPMDEC ' + \
                      'errorL18 magF606 magF814 \n')
    for i in range(0,np.shape(data)[1]) :
      # write line to output file
      string = '{: 15.10f} {: 15.10f} {: 12.5f} {:9.5f} {: 12.5f} ' + \
               '{:9.5f} {: 12.5f} {: 12.5f} {:9.5f} {: 12.5f} {: 12.5f}'
      line = string.format(data[0,i], data[1,i], data[2,i], data[3,i],
                           data[4,i], data[5,i], data[6,i], 
                           data[7,i], data[8,i],data[9,i], data[10,i])
  
      astrometric.write(line)
      astrometric.write("\n")
      
    astrometric.close()

        
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"FITS file reading"
#------------------------------------------------------------------------------

FITS = list()
FITS.append(hst_file_path)
    
for i in range(0, len(FITS)) :
    hdu = fits.open(FITS[i])
    
    if (debug == True) :
        hdu.info()
        print(hdu[1].header)
    
    print("\n Number of stars in HST file: ", len(hdu[1].data))
    
    r_c    = np.asarray(hdu[1].data[:]['r']) #arcsec
    x      = np.asarray(hdu[1].data[:]['x_0']) #arcsec
    y      = np.asarray(hdu[1].data[:]['y_0']) #arcsec
    pmx    = np.asarray(hdu[1].data[:]['pmx(mas/yr)']) #mas/yr
    pmy    = np.asarray(hdu[1].data[:]['pmy(mas/yr)']) #mas/yr  
    epmx   = np.asarray(hdu[1].data[:]['1sig_pmx(mas/yr)']) #mas/yr  
    epmy   = np.asarray(hdu[1].data[:]['1sig_pmx(mas/yr)']) #mas/yr 
    corpmx = np.asarray(hdu[1].data[:]['delta_pmx']) #mas/yr 
    corpmy = np.asarray(hdu[1].data[:]['delta_pmy']) #mas/yr 
    
    F606 = np.asarray(hdu[1].data[:]['m_F606W(mag)']) #mag
    F814 = np.asarray(hdu[1].data[:]['m_F814W(mag)']) #mag

    hdu.close()
    
#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Transform in Gaia/MAMPOSSt-PM shape"
#------------------------------------------------------------------------------

x_rad = (x / 3600) * (np.pi/180)
y_rad = (y / 3600) * (np.pi/180)

RA_rad  = ra_source * (np.pi/180)
Dec_rad = dec_source * (np.pi/180)

v_HST = np.asarray([np.cos(x_rad) * np.cos(y_rad),
                    np.sin(x_rad) * np.cos(y_rad),
                    np.sin(y_rad)])

v0_HST  = np.asarray([np.cos(0) * np.cos(0),
                    np.sin(0) * np.cos(0),
                    np.sin(0)])

v0_GC  = np.asarray([np.cos(RA_rad) * np.cos(Dec_rad),
                    np.sin(RA_rad) * np.cos(Dec_rad),
                    np.sin(Dec_rad)])

RA,DEC = getCoords(v_HST,v0_HST,v0_GC)

#------------------------------------------------------------------------------

narr   = np.asarray([0,0.1,0.5,1])
v_test = np.asarray([np.cos(narr*Rcut*np.pi/180)*np.cos([0,0,0,0]),
                    np.sin(narr*Rcut*np.pi/180)*np.cos([0,0,0,0]),
                    np.sin([0,0,0,0])])

RA_test,DEC_test = getCoords(v_test,v0_HST,v0_GC)

Phi = getPhi(RA_test[1:3],DEC_test[1:3])

RA,DEC = RA * np.pi/180, DEC * np.pi/180
RA,DEC = getCoords(np.asarray([np.cos(RA) * np.cos(DEC),
                    np.sin(RA) * np.cos(DEC),
                    np.sin(DEC)]),v0_GC,v0_GC,np.nanmean(Phi))

if (debug == True) :
    plt.figure(figsize = (5,5))
    hexb=plt.hexbin(RA,DEC,C=np.ones(len(RA)),
                cmap=my_rainbow1,gridsize=50,reduce_C_function=count_function,
                extent=(np.amin(RA),np.amax(RA),
                        np.amin(DEC),np.amax(DEC)))
    plt.gca().invert_xaxis()
    plt.show()
    print("\n Rotated angle after translation [rad]", np.nanmean(Phi))

L18 = np.sqrt(0.5*(epmx*epmx + epmy*epmy) + 0.5*np.abs(epmy*epmy - epmx*epmx))

PMRA, PMDEC   = pmx + pmra_gc, pmy + pmdec_gc
cPMRA, cPMDEC = pmx + corpmx + pmra_gc, pmy + corpmy + pmdec_gc

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Data filtering"
#------------------------------------------------------------------------------

idx_err = np.where(L18 < err_fac * pm_disp)
idx_pm  = np.where((cPMRA-pmra_gc)**2+(cPMDEC-pmdec_gc)**2 < PMcut**2)

idx = np.intersect1d(idx_err,idx_pm)

if (debug == True) :
    R_c = getDistanceDeg(ra_source,dec_source,RA,DEC) * 3600
    print("\n Max diff from Bellini's file and mine [arcsec]:", 
          np.amax(np.abs(r_c - R_c)))

# Gets the indexes
idx, contours = CMD_filter(F606,F814,[3,2,1],nsig,idx)

# Information that will be stored on .dat array
RA     = RA[idx]
DEC    = DEC[idx]
PMRA   = PMRA[idx]
ePMRA  = epmx[idx]
PMDEC  = PMDEC[idx]
ePMDEC = epmx[idx]
PMRAc  = cPMRA[idx]
PMDECc = cPMDEC[idx]
eL18   = L18[idx]
mF606  = F606[idx]
mF814  = F814[idx]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Save data"
#------------------------------------------------------------------------------

data = np.asarray([RA,DEC,PMRA,ePMRA,PMDEC,ePMDEC,PMRAc,PMDECc,eL18,
                   mF606,mF814])

# Writes dataset into .dat file
writeInFile(data)

