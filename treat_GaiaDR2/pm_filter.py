"""
Created on 2020

@author: Eduardo Vitral

"""
###############################################################################
#
# 2020, Paris
#
# This file reads the Gaia DR2 data, keeping only good quality data. 
# Then, it fits the proper motions distributions of both the globular cluster
# and the Milky Way field stars according to Vitral & Mamon 2020b, section 2.3
# and appendix C.2, by means of a maximum likelihood estimation (MLE).
# Finally, it fits the same data by means of an MCMC routine, where the 
# initial departing points are the values previously estimate by the MLE
# routine, plus random Gaussian values with dispersion calcualted from 
# the same MLE parameters. It saves the MCMC chains in a .npy file at 
# the very end.
#
# Documentation is provided on Vitral & Mamon, 2020b. 
# If you have any further questions please email vitral@iap.fr
#
###############################################################################

from astropy.io import fits
import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import minimize
from scipy.optimize import differential_evolution
from scipy import integrate
from scipy.special import gamma
from skimage.feature import peak_local_max
import time
import emcee
from numpy.polynomial.polynomial import polyval2d as P2
    
from matplotlib import rc
rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
## for Palatino and other serif fonts use:
#rc('font',**{'family':'serif','serif':['Palatino']})
rc('text', usetex=True)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Interactive section"
#------------------------------------------------------------------------------

string = "This function was written for the analysis of NGC 6397. " + \
         r"For other globular clusters, the user is invited to fill its" + \
         r" respective values according to the code section" + \
         r" 'Parameters definition'. Note that the .fits file used must " + \
         r"be correctly labeled, please go to the 'FITS file reading' " + \
         r"section of this code to see it.  "
         
print(string)

# Get directory name
string = "Enter the path of your Gaia DR2 .fits file: " + \
         "\n Ex.: /home/usr/DATA/Gaia.fits \n"
gaia_file_path = input(string)

string = "Enter the last digits of the source NGC catalog: " + \
         "\n Ex.: 6397 for NGC6397 \n"
file_name = input(string)

string = "Run on debug mode? (y/n) \n"
debug = input(string)
if (debug == 'y') :
    debug = True
else :
    debug = False

string = "Fraction of PM error (eq. 2 of Vitral & Mamon 2020b): " + \
         "\n Accepted values are 0.25, 0.5 and 1.0 \n"
err_fac = float(input(string))

string = "Select the MLE minimization method: " + \
         "\n Ex.: TNC, for truncated Newton algorithm. " + \
         "\n dif, for differential evolution \n"
min_method = input(string)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"Parameters definition"
#------------------------------------------------------------------------------

# Type here the magnitude cut you want to apply to the data
mag_limit = 30

# In the following, we have correspondences:
# RA       = Source's right ascension in degrees
# DEC_source = Source's declination in degrees
# Rcut       = Maximum radius (in degrees) where we will perform our analysis 
# InG        = Initial guess to be used in the MLE fit: The initial guesses 
#              will be the sum of this array plus the estimated initial guess,
#              described in code section "Guess initial parameters".

if ("6397" in file_name) :
    RA_source  = 265.175375
    DEC_source = -53.674333
    pm_disp    = 0.394
    Rcut       = 1
    InG_sum    = np.asarray([0.5,0,0,0,0,0,0,0])

# Defines the source's center in (RA,Dec) space
CM = np.asarray([RA_source,DEC_source])

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"MCMC routine"
#---------------------------------------------------------------------------

# MCMC routine function
# ndim: 8 (number of free parameters of our model)
# nwalkers: 20 (number of chains)
# steps: 200000 (number of MCMC steps/iterations)
def MCMC (ndim,nwalkers,steps,MLE_guess,lnprob) :
    gauss_ball = np.asarray([sig1*0.1,sig1*0.1,sig1*0.01,
                             b2*0.1,b2*0.1,b2*0.01,d2*0.01,f*0.01])
    
    pos = [MLE_guess + \
           gauss_ball*np.random.randn(ndim) for i in range(nwalkers)]
    sampler = emcee.EnsembleSampler(nwalkers, ndim, lnprob, args=())
    sampler.run_mcmc(pos, steps)
    
    return sampler.chain

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"Proper motion PDFs and statistical analysis"
#---------------------------------------------------------------------------

# This function projects the PM distribution function in one dimension
# x: PMRA or PMDec / mu1: globular cluster mean / mu2: field stars mean
def DistXY(x,mu1,mu2,sig1,b2,d2,f) :
    # PDF from globular cluster
    f1    = f*np.exp(-np.power(x-mu1,2)/(2*sig1*sig1))/np.sqrt(2*np.pi * \
                    sig1*sig1)
    # PDF from field stars
    Dist2 = -(1-f)*((2+d2)/b2)*(1+((x-mu2)/b2)*((x-mu2)/b2))**((1+d2)/2) \
        * gamma(-1/2-d2/2)/(2*gamma(-d2/2)*np.sqrt(np.pi))
      
    return (f1+Dist2)

# This function converts the coefficients of the field stars proper motion
# filtering to a suitable form and then calculates the PDF of the field
# stars according to the method described in Vitral & Mamon, 2020b
def pFS_log(b2,d2,err2,R0) :
    X = R0/b2
    
    A  = getPoly(d2,np.log10(np.sqrt(err2)/b2),A_log)
    B  = -10**(getPoly(d2,np.log10(np.sqrt(err2)/b2),B_log))
    X1 = 10**(getPoly(d2,np.log10(np.sqrt(err2)/b2),X1_log))
    X2 = 10**(getPoly(d2,np.log10(np.sqrt(err2)/b2),X2_log))
    y0 = 10**(getPoly(d2,np.log10(np.sqrt(err2)/b2),y0_log))
    g  = -10**(getPoly(d2,np.log10(np.sqrt(err2)/b2),g_log))
    
    ratio = ((y0*(1+(X/X1)**A))**g + (1+(X/X2)**B)**g)**(1/g)
    pconv = - ratio * (d2+2) * (X/b2) * (1+X**2)**(d2/2)
    return pconv

# This function gives the sum of the PDF's from GC and field stars
# Ux: PMRA / Uy: PMDec / mu_1: GC's mean / mu_2: field stars mean
# b2: field stars scale radius in PM / d2: field stars slope in PM
def Dist2d(Ux,ex,Uy,ey,cov,mux1,muy1,sig1,mux2,muy2,b2,d2,f) :
    # Distance from GC's mean in PM space
    rPM1 = np.sqrt((Ux-mux1)**2+(Uy-muy1)**2)
    # Distance from field stars mean in PM space
    rPM2 = np.sqrt((Ux-mux2)**2 + (Uy-muy2)**2)
    
    # Globular cluster error array
    err1 = (ex*(Ux-mux1)/rPM1)**2 + (ey*(Uy-muy1)/rPM1)**2 + \
            2*ex*ey*cov*(Ux-mux1)*(Uy-muy1)/rPM1**2
    # Field stars error array
    err2 = (ex*(Ux-mux2)/rPM2)**2 + (ey*(Uy-muy2)/rPM2)**2 + \
            2*ex*ey*cov*(Ux-mux2)*(Uy-muy2)/rPM2**2
    
    sig1 = np.sqrt(sig1**2 + err1)
    
    # PDF from GC
    Gauss1 = f * rPM1 * np.exp(-np.power(rPM1/sig1,2)/2) / (sig1*sig1)
    # PDF from field stars
    Dist2  = (1-f) * pFS_log(b2,d2,err2,rPM2) 
           
    return (Gauss1 + Dist2)

# This function calculates the maximum likelihood
# Index 1 refers to GC / Index 2 refers to field stars
# x refers to PMRA / y refers to PMDec
def MLE_Dist2d(params) :
    
    mux1 = params[0]
    muy1 = params[1]
    sig1 = params[2]
    
    mux2 = params[3]
    muy2 = params[4]
    b2   = params[5]
    d2   = params[6]
    
    frac = params[7]
    
    # Gets the PDF of the ensemble
    f_i = Dist2d(PMRA,ePMRA,PMDEC,ePMDEC,ePMDR,
                 mux1,muy1,sig1,mux2,muy2,b2,d2,frac)
    # Transforms zero's in NaN
    f_i[f_i == 0] = np.nan
    
    # Calculates the ML, taking out NaN's
    L = -np.sum(np.log(f_i[np.logical_not(np.isnan(f_i))]))
    
    if (debug == True) :
        print("Parameters", params)
        print("Min value of the PDF", np.nanmin(f_i))
        print("Min value of the PDF's log", np.nanmin(np.log(f_i)))
        print("Maximum likelihood: ", L)
    
    return L


# This function gets the prior assumptions on the parameters
# Attention: The priors are caracteristic for each cluster
def lnprior(params):
    mu1x = params[0]
    mu1y = params[1]
    sig11 = params[2]
    
    mu2x = params[3]
    mu2y = params[4]
    b22   = params[5]
    d22   = params[6]
    
    frac = params[7]
    
    if ((mux1-sig1 < mu1x < mux1+sig1) and \
        (muy1-sig1 < mu1y < muy1+sig1) and \
        (0.5*sig1 < sig11 < 2*sig1) and \
        (mux2-b2 < mu2x < mux2+b2) and \
        (muy2-b2 < mu2y < muy2+b2) and \
        (0.5*b2 < b22 < 2*b2) and \
        (-8 < d22 < -3) and (0 < frac < 1)) :
        return 0.0
    return -np.inf

# This function gets the prior probability for MCMC
def lnprob(params):
    lp = lnprior(params)
    if not np.isfinite(lp):
        return -np.inf
    return lp - MLE_Dist2d(params)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"Guess initial parameters"    
#---------------------------------------------------------------------------

# This function estimates the dispersion of the GC Gaussian in PM space
# x_axis: PMRA or PMDec / gauss: Histogram / peak: PMRA or PMDec in the peak
def oneSigma (x_axis, gauss, peak) :
    # Gets the array's index where the peak is located
    i, j = np.argmin(np.abs(x_axis-peak)), np.argmin(np.abs(x_axis-peak))
            
    # Gets the histogram max value times e^(-1/2): (histogram value at 1 sigma)
    threshold = gauss[i]*np.exp(-1/2)
    
    # Search where the histogram is less than the threshold
    while (gauss[i] >= threshold) :
        i -= 1
    index_left = i+1
    while (gauss[j] >= threshold) :
        j += 1
    index_right = j
    
    # Assigns sigma as the minimum between the distance took to depass the
    # threshold in the left and in the right of the peak
    sigma = min(x_axis[index_right]-peak, peak-x_axis[index_left])
    
    return sigma

# This function esitmates the fraction of stars belonging to each PM clump
# mean, sigma : 0 --> clump 0 / 1 --> clump 1
# x_axis: PMRA / y_axis: PMDec
def Flux_fraction(mean, sigma, x_axis, y_axis) :
    
    index_left  = np.argmin(np.abs(x_axis-(mean[0]-3*sigma[0])))
    index_right = np.argmin(np.abs(x_axis-(mean[0]+3*sigma[0])))
    # Flux of stars belonging to the clump 0
    flux1 = integrate.simps(y_axis[index_left:index_right], \
                            x_axis[index_left:index_right])

    index_left  = np.argmin(np.abs(x_axis-(mean[1]-3*sigma[1])))
    index_right = np.argmin(np.abs(x_axis-(mean[1]+3*sigma[1])))
    # Flux of stars belonging to the clump 1
    flux2       = integrate.simps(y_axis[index_left:index_right], \
                            x_axis[index_left:index_right])
    
    return np.asarray([flux1/(flux1+flux2),flux2/(flux1+flux2)])

# This function estimates the initial parameters to be given to the MLE
# x_data: PMRA / y_data: PMDec
def GuessIn (x_data, y_data) :
    
    # Takes off NaN values
    x_data = x_data[np.logical_not(np.isnan(x_data))]
    y_data = y_data[np.logical_not(np.isnan(y_data))]
    
    # Gets the histogram in PMRA
    x_hist, x_axis = np.histogram(x_data, bins = 5000, \
                                  range = (np.amin(x_data),np.amax(x_data)))
    x_axis = np.delete(x_axis,0)
    
    # Gets the histogram in PMDec
    y_hist, y_axis = np.histogram(y_data, bins = 5000, \
                                  range = (np.amin(y_data),np.amax(y_data)))
    y_axis = np.delete(y_axis,0)
    
    # Gets the histogram of the 2d (PMRA,PMDec) data
    hist, xedges, yedges = np.histogram2d(x_data, y_data, bins=1000)
    hist = hist.T
    
    # Estimates the PMRA and PMDec means from the two clumps by taking the 
    # two main local maxima of the 2d histogram. With that, it estimates the
    # other clump parameters
    peaks          = peak_local_max(hist, num_peaks=2)
    y_peak, x_peak = peaks.T[0], peaks.T[1]
    mean_x, mean_y = xedges[x_peak], yedges[y_peak]
    sigma_x        = np.asarray([oneSigma(x_axis,x_hist,mean_x[0]), \
                          oneSigma(x_axis,x_hist,mean_x[1])])
    sigma_y        = np.asarray([oneSigma(y_axis,y_hist,mean_y[0]), \
                          oneSigma(y_axis,y_hist,mean_y[1])])
    frac_x         = Flux_fraction(mean_x, sigma_x, x_axis, x_hist)
    frac_y         = Flux_fraction(mean_y, sigma_y, y_axis, y_hist)
    
    # Tries to find the GC assuming it is the clump with the smaller dispersion 
    if (min(sigma_x[0],sigma_y[0]) < min(sigma_x[1],sigma_y[1])) :
        mux1 = mean_x[0]
        muy1 = mean_y[0]
        sig1 = min(sigma_x[0],sigma_y[0])
        
        mux2 = mean_x[1]
        muy2 = mean_y[1]
        b2   = max(sigma_x[1],sigma_y[1])
    else :
        mux1 = mean_x[1]
        muy1 = mean_y[1]
        sig1 = min(sigma_x[1],sigma_y[1])
        
        mux2 = mean_x[0]
        muy2 = mean_y[0]
        b2   = max(sigma_x[0],sigma_y[0])
    # Assigns the GC fraction of stars as the minimum one between al the clumps
    f = min(min(frac_x[0],frac_y[0]),min(frac_x[1],frac_y[1]))
    
    # Returns the initial values for the MLE, assuming the field stars PM slope
    # to begin as -6
    return (np.asarray([mux1,muy1,sig1,mux2,muy2,2*b2,-6,f]) + InG_sum)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"General functions"
#---------------------------------------------------------------------------
# Read columns in a file
def ReadColumns (file) :
    words = np.loadtxt(file)
    return words 

def getCoeffLOG (file) :
    words  = ReadColumns(file)
    ni, nj = 7, 7
    #line i stands for the slope and column j stands for err/A_PM
    matrix = np.zeros((ni,nj))
    for i in range (0,ni) :
        for j in range (0,nj) :
            matrix[i,j] = words[i][j]
    return matrix

# Does a matrix polynomial sum in a efficient way
def getPoly(x,y,coefficient_matrix):
    z = P2(x, y, coefficient_matrix)
    return z

# Gets the distance (in degrees) between a star and the GC center
def GetR (RA, DEC) :
    R = (180/np.pi)*np.arccos(np.sin(DEC*np.pi/180) * \
         np.sin(CM[1]*np.pi/180) + np.cos(DEC*np.pi/180) * \
         np.cos(CM[1]*np.pi/180)*np.cos((RA-CM[0])*np.pi/180))

    return np.asarray(R)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"FITS files upload"
#---------------------------------------------------------------------------

FITS = list()
FITS.append(gaia_file_path)
    
for i in range(0, len(FITS)) :
    hdu = fits.open(FITS[i])
    
    if (debug == True) :
        hdu.info()
        print(hdu[1].header)
    
    print("\n Number of stars in Gaia DR2 file: ", len(hdu[1].data))
    
    RA      = np.asarray(hdu[1].data[:]['ra      '])  #degrees
    DEC     = np.asarray(hdu[1].data[:]['DEC     ']) #degrees
    pmra    = np.asarray(hdu[1].data[:]['pmra    '])  #mas/yr
    epmra   = np.asarray(hdu[1].data[:]['pmra_error'])  #mas/yr
    pmdec   = np.asarray(hdu[1].data[:]['pmdec   ']) #mas/yr
    epmdec  = np.asarray(hdu[1].data[:]['pmdec_error'])  #mas/yr
    epmdr   = np.asarray(hdu[1].data[:]['pmra_pmdec_corr'])  #mas/yr
    g_mag   = np.asarray(hdu[1].data[:]['phot_g_mean_mag']) # in mag
    b_rp    = np.asarray(hdu[1].data[:]['bp_rp   ']) # in mag
    SNR_par = np.asarray(hdu[1].data[:]['parallax_over_error']) # no units
    ast_err = np.asarray(hdu[1].data[:]['astrometric_excess_noise']) # no units
    chi2    = np.asarray(hdu[1].data[:]['astrometric_chi2_al']) # no units
    nu = np.asarray(hdu[1].data[:]['astrometric_n_good_obs_al']) # no units
    E  = np.asarray(hdu[1].data[:]['phot_bp_rp_excess_factor']) # no units
    
    stars   = np.asarray([RA,DEC,pmra,epmra,pmdec,epmdec,epmdr,
                          g_mag,b_rp,SNR_par])
    
    u = np.sqrt(chi2/(nu-5))
    

    hdu.close()

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"Fitting procedure"
#---------------------------------------------------------------------------

# Gets field stats' proper motion fitting coefficients
y0_log = getCoeffLOG('y0coeffs_short.dat')
A_log  = getCoeffLOG('acoeffs_short.dat')
B_log  = getCoeffLOG('bcoeffs_short.dat')
X1_log = getCoeffLOG('X1coeffs_short.dat')
X2_log = getCoeffLOG('X2coeffs_short.dat')
g_log  = getCoeffLOG('gcoeffs_short.dat')

# Gets the initial guess of the parameters
InG    = GuessIn(pmra,pmdec)
string = 'PMRA_0,GC ; PMDEC_0,GC ; sig_GC ; PMRA_0,FS ; PMDEC_0,FS ; b_FS ;' \
       + ' d_FS ; Fraction_GC'
print('\n Initial guesses (' + string + '): \n ', InG)

# Apply filters
idx1 = np.where(GetR(RA, DEC) < Rcut)
idx2 = np.where(g_mag < mag_limit)
idx  = np.intersect1d(idx1,idx2)
    
# Gets stars with negligeable PM errors, according to the definition of
# the proper motion major axis error, given in Lindegren et al., 2018
c33 = stars[3]**2
c34 = stars[3] * stars[5] * stars[6]
c44 = stars[5]**2
err = np.sqrt(0.5*(c33+c44) + 0.5*np.sqrt((c44-c33)**2 + 4*c34**2))

idx_err  = np.where(err < err_fac * pm_disp)
idx      = np.intersect1d(idx,idx_err)

idx_noise = np.where(ast_err < 1)

idx_noise1 = np.where(u < 1.2*np.maximum(1,np.exp(-0.2*(g_mag-19.5))))
idx_noiseE1 = np.where(1.0 + 0.015 * b_rp**2 < E)
idx_noiseE2 = np.where(E < 1.3 + 0.06 * b_rp**2)
idx_noise2 = np.intersect1d(idx_noiseE1,idx_noiseE2)

idx_noise  = np.intersect1d(idx_noise1,idx_noise2)

idx       = np.intersect1d(idx,idx_noise)

string = '\n Number of stars in new filtered subset' + \
         ' (according to data quality): '
print(string, len(idx))

PMRA   = pmra[idx]
ePMRA  = epmra[idx]

PMDEC  = pmdec[idx] 
ePMDEC = epmdec[idx]

ePMDR  = epmdr[idx]

x  = np.copy(PMRA)
ex = np.copy(ePMRA)

y  = np.copy(PMDEC)
ey = np.copy(ePMDEC)

if (min_method == 'TNC') :
    MLE_model  = minimize(MLE_Dist2d, np.asarray(InG), method='TNC',
                          bounds = [(None,None), (None,None), (0.001,None), 
                                    (None,None), (None,None), (0.001,None), 
                                    (-8,-3), (0.01,1)])
    results_MLE = MLE_model['x']
    
if (min_method == 'dif') :
    InG    = InG - InG_sum
    bounds = [(InG[0]-3*InG[2],InG[0]+3*InG[2]),
              (InG[1]-3*InG[2],InG[1]+3*InG[2]),
              (0.1*InG[2],10*InG[2]),
              (InG[3]-5*InG[5],InG[3]+5*InG[5]),
              (InG[4]-5*InG[5],InG[4]+5*InG[5]),
              (0.1*InG[5],10*InG[5]),
              (-8,-3), (0.01,1)]
    MLE_model   = differential_evolution(MLE_Dist2d, bounds)
    results_MLE = MLE_model.x

mux1 = results_MLE[0]
muy1 = results_MLE[1]
sig1 = results_MLE[2]

mux2 = results_MLE[3]
muy2 = results_MLE[4]
b2   = results_MLE[5]
d2   = results_MLE[6]

f    = results_MLE[7]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#---------------------------------------------------------------------------
"Prints histograms"
#---------------------------------------------------------------------------

my_grey = (240/255,240/255,240/255)

ranges  = [[min(InG[0],InG[3]) - 3*max(InG[2],InG[5]), max(InG[0],InG[3]) + \
          3*max(InG[2],InG[5])],
          [min(InG[1],InG[4]) - 3*max(InG[2],InG[5]),max(InG[1],InG[4]) + \
          3*max(InG[2],InG[5])]]
domains = np.asarray([np.linspace(ranges[0][0],ranges[0][1],1000),
                      np.linspace(ranges[1][0],ranges[1][1],1000)])

xlabels = [r"$\mu_{\alpha,*} \ [\mathrm{mas \ yr}^{-1}]$",
           r"$\mu_{\delta} \ [\mathrm{mas \ yr}^{-1}]$"]

ylabels = [r"$\mathrm{Histogram} - \mu_{\alpha,*}$",
           r"$\mathrm{Histogram} - \mu_{\delta}$"]

fig, axs = plt.subplots(2,1, figsize=(6,7), facecolor='w', edgecolor='k')
fig.subplots_adjust(hspace = .4, wspace=.15)

axs[0].hist(x, 500, range=ranges[0], alpha=1,density=True,
         histtype='stepfilled', color=(102/255,178/255,1))
axs[0].plot(domains[0], DistXY(domains[0],mux1,mux2,sig1,b2,d2,f), 
         color='darkgreen', lw=4,
         label='$\mathrm{MLE \ Fit}$')

axs[1].hist(y, 500, range=ranges[1], alpha=1, density=True,
         histtype='stepfilled', color=(1,102/255,102/255))
axs[1].plot(domains[1], DistXY(domains[1],muy1,muy2,sig1,b2,d2,f), 
         color='darkgreen', lw=4,
         label='$\mathrm{MLE \ Fit}$')

for i in range(0,len(xlabels)): 
    axs[i].tick_params(labeltop=False, labelright=False, top = True, 
                       right = True, 
                    axis='both', which='major', labelsize=14,
                    direction="in", 
                    length = 8)
    axs[i].tick_params(labeltop=False, labelright=False, top = True, 
                       right = True, 
                    axis='both', which='minor', labelsize=14,
                    direction="in",
                    length = 4)
    axs[i].set_ylabel(ylabels[i], fontsize=17)
    axs[i].set_xlabel(xlabels[i], fontsize = 16)
    axs[i].set_xlim(ranges[i])
    axs[i].set_facecolor(my_grey)
    axs[i].grid()
    axs[i].minorticks_on()
    axs[i].legend(prop={'size': 14})
    
plt.show()

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#------------------------------------------------------------------------------
"MCMC routine"
#------------------------------------------------------------------------------

t2_start = time.perf_counter()
sample = MCMC(8,20,200000,results_MLE,lnprob)
np.save('MCMC_PM_chain', sample) 
print("\n Finished MCMC: ", round((time.perf_counter()-t2_start)/3600,3), 
      " hours")